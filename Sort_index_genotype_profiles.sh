cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_filtered_MAF_INDELS/
 
for NAME in $(find . -name "Imputed_Monocyte_genotype_*" -printf "%f\n" | sed 's/.recode.vcf//'); do
for ADDRESS in $(find . -name $NAME'.recode.vcf'); do  

echo $NAME
echo $ADDRESS

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add python/2.7.5
module add samtools 
module add vcftools 

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_filtered_MAF_INDELS/

#--- sort vcf
vcf-sort' $NAME'.recode.vcf >' $NAME'.recode_sorted.vcf 

#--- creat index files
bgzip' $NAME'.recode_sorted.vcf && tabix -p vcf' $NAME'.recode_sorted.vcf.gz' > /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_filtered_MAF_INDELS/script/'create_index_'$NAME'.sh'

done
done

