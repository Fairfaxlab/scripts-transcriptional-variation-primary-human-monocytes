
#--------------- QTLtools
#ssh cbrglogin1
module load qtltools   
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/SDA_IFN_LPS_RNAseq/
QTLtools trans --vcf Genotype.vcf.gz --bed SDA_450.bed.gz --out SDA_results_COM450 --normal --nominal --threshold 0.05 --window 1000000
