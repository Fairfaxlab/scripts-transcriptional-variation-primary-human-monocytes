#------------------------------- create summary statistics
cd /t1-data/user/nassisar/HISAT/
 
for NAME in $(find . -name '*_nodup_properPairs_NH.bam' -printf "%f\n" | sed 's/_nodup_properPairs_NH.bam//'); do
for ADDRESS in $(find . -name $NAME'_nodup_properPairs_NH.bam' | sed 's/_nodup_properPairs_NH.bam//'); do 

echo "$NAME"
echo "$ADDRESS"

DIR=/t1-data/user/nassisar/HISAT/flagstat/

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar 
#$ -m eas

module add samtools

cd /t1-data/user/nassisar/HISAT/

samtools flagstat' $ADDRESS'_nodup_properPairs_NH.bam > /t1-data/user/nassisar/HISAT/flagstat/'$NAME'_flagstat.txt' > '/t1-data/user/nassisar/HISAT/flagstat/'$NAME'_nodup_flagstat.sh'
 
done
done
