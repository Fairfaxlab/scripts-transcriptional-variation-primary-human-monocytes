
#SDA analysis
ssh cbrglogin1
/home/fairfaxlab/nassisar/Desktop/qsub_files/Step66_SDA/sda_static_linux --data /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/SDA_IFN_LPS_RNAseq/Expression-IFN_LPS_RNAseq.txt --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/SDA_IFN_LPS_RNAseq/SDA_results_COM500/ --num_comps 450 --N 130
 
# N =  individuals, 
# L =  genes 
# T =  contexts.
# C : component
# 
# rows : N
# col : L
# Using this value of N and the number of lines in input, the code calculates
# the number of contexts (T)
# 
# Context score per component: Activity based matrix (B1): contains the context scores matrix with T rows and C columns.
# Individual score per component: A : Posterior mean of the individual scores matrix (N rows, C columns).
# Gene score per component (or gene loadings): Gene score matrix (X1) : Posterior mean of the loadings matrix (C rows, L columns).
# 