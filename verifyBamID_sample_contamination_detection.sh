
declare x=0  
p1='_1.fastq.gz'
p2='_2.fastq.gz'

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/rawData/
while read -r -a line; do
echo ${line[0]}
echo ${line[1]}

for NAME in $(find . -name ${line[0]}'_1.fastq.gz' -printf "%f\n" | sed 's/_1.fastq.gz//'); do
for ADDRESS in $(find . -name $NAME'_1.fastq.gz' | sed 's/_1.fastq.gz//'); do 
line=$ADDRESS
echo $x
    if [ "$x" -eq 0 ]; then 
    var1="$line"; fi
    if [ "$x" -eq 1 ]; then 
    var2="$line"; fi
    if [ "$x" -eq 2 ]; then 
    var3="$line"; fi
    if [ "$x" -eq 3 ]; then 
    var4="$line"; fi
    if [ "$x" -eq 4 ]; then 
    var5="$line"; fi
    if [ "$x" -eq 5 ]; then 
    var6="$line"; fi
    if [ "$x" -eq 6 ]; then 
    var7="$line"; fi
    if [ "$x" -eq 7 ]; then 
    var8="$line";fi
    if [ "$x" -eq 8 ]; then 
    var9="$line";fi
    if [ "$x" -eq 9 ]; then 
    var10="$line";fi
    if [ "$x" -eq 10 ]; then 
    var11="$line";fi
    if [ "$x" -eq 11 ]; then 
    var12="$line";fi
    if [ "$x" -eq 12 ]; then 
    var13="$line";fi
    if [ "$x" -eq 13 ]; then 
    var14="$line";fi
    if [ "$x" -eq 14 ]; then 
    var15="$line";fi
    if [ "$x" -eq 15 ]; then 
    var16="$line";fi
    if [ "$x" -eq 16 ]; then 
    var17="$line";fi
    if [ "$x" -eq 17 ]; then 
    var18="$line";fi
    if [ "$x" -eq 18 ]; then 
    var19="$line";fi

    x=$((x + 1))

   if [ "$x" -eq 19 ]; then

i="$NAME"
echo "$ADDRESS"

DIR1=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/secondcandidate_DIFF/$i/
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/secondcandidate_DIFF_with_best/$i/
mkdir /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/secondcandidate_DIFF_with_best/${i}

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas
rm' $DIR$i.'bam; 
chmod u+x /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/verifyBamID.20120620
/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/verifyBamID.20120620 --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/crossmap/CrossMap-0.2.7/Monocyte_genotype_Hg38_subset.vcf.gz --bam' $DIR1$i'_nodup_properPairs_NH.bam --best --maxDepth 500 --precise --out' $DIR$i'.selfSM' $DIR$i'.depthSM' $DIR$i'.selfRG' $DIR$i'.depthRG' $DIR$i'.bestSM' $DIR$i'.bestRG --verbose' > /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_project/verifyBamfiles/secondcandidate_DIFF_with_best/script_with_best_19/$i.best.sh

x=0
fi

done
done
done < '/t1-home/fairfaxlab/nassisar/Desktop/Monocyte/input_problematic_19.txt'
 
 
