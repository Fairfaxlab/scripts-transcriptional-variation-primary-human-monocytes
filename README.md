# README #

This web page accompanies the manuscript titled 'Genetic Determinants of Transcriptional Variation in Primary Human Monocytes Across Multiple Contexts', by Fairfax et al, which has been published in ---. If you want to use any of the cis-eQTL results displayed on this page in your publication, please cite this paper and related puplication. For further questions, contact the corresponding author: benjamin.fairfax@oncology.ox.ac.uk.

### What is this repository for? ###

We have made this repository available for Scripts used in the analysis and figure synthesis.

### How do I get set up? ###

You can find step by step instructions inside of each script. 