DIR=/t1-data/user/nassisar/HISAT/
OUTPUT=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_bigwing/

cd /t1-data/user/nassisar/HISAT/
for NAME in $(find . -name "*_nodup_properPairs_NH.bam" -printf "%f\n" | sed 's/_nodup_properPairs_NH.bam//'); do
for ADDRESS in $(find . -name $NAME'_nodup_properPairs_NH.bam'); do 

echo "$NAME"
echo "$ADDRESS"

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add deeptools/3.0.1

cd /t1-data/user/nassisar/HISAT/
bamCoverage -b' $ADDRESS '-o' $OUTPUT$NAME'.bw' > /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Monocyte_bigwing/scripts/'bamCoverage_'$NAME.sh

done 
done
