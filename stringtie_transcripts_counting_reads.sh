 
cd /t1-data/user/nassisar/melanomaCohort/concerned_samples/ 

#create mergelist
find . -name '*_nodup_properPairs_NH.gtf' >> /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/mergelist.txt

#Create merged gtf file
module add stringtie
module add hisat
stringtie --merge -p 8 -G /t1-data/user/nassisar/HISAT/GENOMEANNOT/GRCh38.gtf -o '/t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/stringtie_merged.gtf' /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/mergelist.txt
#-----------

DIR=/t1-data/user/nassisar/melanomaCohort/concerned_samples/ 
cd $DIR
for NAME in $(find . -name "*_nodup_properPairs_NH.gtf" -printf "%f\n" | sed 's/_nodup_properPairs_NH.gtf//'); do
echo "$NAME"
for i in `ls $NAME'_nodup_properPairs_NH.bam'`; do
echo $DIR$i
 
#Run stringtie
mkdir '/t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/'$NAME

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas
module add stringtie
module add python
cd /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/'$NAME'; 
stringtie '$DIR$i '-G /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/stringtie_merged.gtf -A gene_abund.txt -e -B -p 8 > /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/'$NAME'/'$NAME'.gtf' > /t1-data/user/nassisar/melanomaCohort/concerned_samples/TP_readcount/$NAME.sh

done
done


