
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/melanomaCohort/5hmC_data/Results_mapping/mapping_2019_samples/
OUTPUT=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/melanomaCohort/5hmC_data/Results_mapping/mapping_2019_samples//readcount_gene/

cd $DIR
for NAME in $(find . -name "*_nodup_properPairs_NH.bam" -printf "%f\n" | sed 's/_nodup_properPairs_NH.bam//'); do
for ADDRESS in $(find . -name $NAME'_nodup_properPairs_NH.bam'); do 

echo "$NAME"
echo "$ADDRESS"

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add HTSeq/0.6.1p1
module add python

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/melanomaCohort/5hmC_data/Results_mapping/mapping_2019_samples/
python -m HTSeq.scripts.count --format=bam --minaqual=0 --stranded=no --type=exon --mode=union' $ADDRESS '/t1-data/user/nassisar/HISAT/GENOMEANNOT/GRCh38.gtf > /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/melanomaCohort/5hmC_data/Results_mapping/mapping_2019_samples//readcount_gene//'$NAME'_gene_read_count.txt' > $OUTPUT'CD_'$NAME'.sh'

done 
done



