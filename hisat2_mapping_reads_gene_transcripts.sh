
DATADIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/
OUTPUTDIR=/t1-data/user/nassisar/HISAT/NEW_SAMPLES_19/

filename='/t1-home/fairfaxlab/nassisar/Desktop/Monocyte/NEW_SAMPLES_19.txt'
filelines=`cat $filename`

declare x=0  

p1='_1.fastq.gz'
p2='_2.fastq.gz'

for i in $filelines ; do
cd $DATADIR
for line in $(find . -name $i"_1.fastq.gz" | sed 's/_1.fastq.gz//' ); do 

    if [ "$x" -eq 0 ]; then 
    var1="$line"; fi
    if [ "$x" -eq 1 ]; then 
    var2="$line"; fi
    if [ "$x" -eq 2 ]; then 
    var3="$line"; fi
    if [ "$x" -eq 3 ]; then 
    var4="$line"; fi
    if [ "$x" -eq 4 ]; then 
    var5="$line"; fi
    if [ "$x" -eq 5 ]; then 
    var6="$line"; fi
    if [ "$x" -eq 6 ]; then 
    var7="$line"; fi
    if [ "$x" -eq 7 ]; then 
    var8="$line";fi
    if [ "$x" -eq 8 ]; then 
    var9="$line";fi
    if [ "$x" -eq 9 ]; then 
    var10="$line";fi
    if [ "$x" -eq 10 ]; then 
    var11="$line";fi
    if [ "$x" -eq 11 ]; then 
    var12="$line";fi
    if [ "$x" -eq 12 ]; then 
    var13="$line";fi
    if [ "$x" -eq 13 ]; then 
    var14="$line";fi
    if [ "$x" -eq 14 ]; then 
    var15="$line";fi
    if [ "$x" -eq 15 ]; then 
    var16="$line";fi
    if [ "$x" -eq 16 ]; then 
    var17="$line";fi
    if [ "$x" -eq 17 ]; then 
    var18="$line";fi
    if [ "$x" -eq 18 ]; then 
    var19="$line";fi

    x=$((x + 1))

   if [ "$x" -eq 19 ]; then

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add hisat/2.1.0
module add picard-tools
module add stringtie
module add bcftools
module add samtools

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/ 

hisat2 -x /t1-data/user/nassisar/HISAT/GENOMEANNOT/GRCh38.dna -p 8 -1' $var1$p1 '-2' $var1$p2 '-1' $var2$p1 '-2' $var2$p2 '-1' $var3$p1 '-2' $var3$p2 '-1' $var4$p1 '-2' $var4$p2 '-1' $var5$p1 '-2' $var5$p2 '-1' $var6$p1 '-2' $var6$p2 '-1' $var7$p1 '-2' $var7$p2 '-1' $var8$p1 '-2' $var8$p2 '-1' $var9$p1 '-2' $var9$p2 '-1' $var10$p1 '-2' $var10$p2 '-1' $var11$p1 '-2' $var11$p2 '-1' $var12$p1 '-2' $var12$p2 '-1' $var13$p1 '-2' $var13$p2 '-1' $var14$p1 '-2' $var14$p2 '-1' $var15$p1 '-2' $var15$p2 '-1' $var16$p1 '-2' $var16$p2 '-1' $var17$p1 '-2' $var17$p2 '-1' $var18$p1 '-2' $var18$p2 '-1' $var19$p1 '-2' $var19$p2 '| samtools view -bS - >' $OUTPUTDIR$i'.bam

samtools sort --threads 8' $OUTPUTDIR$i'.bam >' $OUTPUTDIR$i'_sorted.bam 
MarkDuplicates I='$OUTPUTDIR$i'_sorted.bam O='$OUTPUTDIR$i'_nodup.bam M='$OUTPUTDIR$i'_marked_dup_metrics.txt REMOVE_DUPLICATES=true
samtools flagstat' $OUTPUTDIR$i'.bam >' $OUTPUTDIR$i'_first_bam_flagstat.txt
samtools flagstat' $OUTPUTDIR$i'_nodup.bam >' $OUTPUTDIR$i'_nodup_flagstat.txt
rm -rf' $OUTPUTDIR$i'_sorted.bam
bamtools filter -tag NH:1 -in' $OUTPUTDIR$i'_nodup.bam | bamtools filter -isProperPair true -out' $OUTPUTDIR$i'_nodup_properPairs_NH.bam
samtools flagstat' $OUTPUTDIR$i'_nodup_properPairs_NH.bam >' $OUTPUTDIR$i'_nodup_properPairs_NH_flagstat.txt 
BuildBamIndex I='$OUTPUTDIR$i'_nodup_properPairs_NH.bam
rm -rf' $OUTPUTDIR$i'_nodup.bam

stringtie -p 8 -G /t1-data/user/nassisar/HISAT/GENOMEANNOT/GRCh38.gtf -o' $OUTPUTDIR$i'_nodup_properPairs_NH.gtf' $OUTPUTDIR$i'_nodup_properPairs_NH.bam

samtools mpileup -g -f /t1-data/user/nassisar/HISAT/GENOMEANNOT/GRCh38.fa' $OUTPUTDIR$i'_nodup_properPairs_NH.bam >' $OUTPUTDIR$i'_nodup_properPairs_NH.bcf
bcftools call -m -v -O z -o' $OUTPUTDIR$i'_nodup_properPairs_NH.vcf.gz' $OUTPUTDIR$i'_nodup_properPairs_NH.bcf --threads 8
rm' $OUTPUTDIR$i'_nodup_properPairs_NH.bcf
bcftools view' $OUTPUTDIR$i'_nodup_properPairs_NH.vcf.gz | vcfutils.pl varFilter - >' $OUTPUTDIR$i'_nodup_properPairs_NH_filtered.vcf' > /t1-data/user/nassisar/HISAT/NEW_SAMPLES_19/script/'HISAT'$i.sh

    x=0
    fi

done
done
